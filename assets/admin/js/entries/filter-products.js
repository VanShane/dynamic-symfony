// import { removeMessage } from '../functions';
import noUiSlider from 'nouislider';
// import Filter from '../../../common/js/modules/Filter';


window.onload = () => {
    // removeMessage();

    const priceSlider = document.getElementById('price-slider');
    let minPrice = Math.floor(parseInt(priceSlider.parentElement.dataset.min, 10) / 10) * 10;
    let maxPrice = Math.ceil(parseInt(priceSlider.parentElement.dataset.max, 10) / 10) * 10;
    const minPriceInput = document.getElementById('minPrice');
    const maxPriceInput = document.getElementById('maxPrice');

    const priceRange = noUiSlider.create(priceSlider, {
        start: [minPrice, maxPrice],
        connect: true,
        step: 100,
        range: {
            'min': minPrice,
            'max': maxPrice
        }
    });

    const stockSlider = document.getElementById('stock-slider');
    let minStock = parseInt(stockSlider.parentElement.dataset.min);
    let maxStock = parseInt(stockSlider.parentElement.dataset.max);
    const minStockInput = document.getElementById('minStock');
    const maxStockInput = document.getElementById('maxStock');

    const stockRange = noUiSlider.create(stockSlider, {
        start: [minStock, maxStock],
        connect: true,
        step: 100,
        range: {
            'min': minStock,
            'max': maxStock
        }
    });

    function rangeEvents(range, slider, min, max) {
        range.on('slide', (values, handle) => {
            if (handle === 0) {
                slider.parentElement.dataset.min = Math.round(values[0]);
                min.value = Math.round(values[0]);
            }
            if (handle === 1) {
                slider.parentElement.dataset.max = Math.round(values[1]);
                max.value = Math.round(values[1]);
            }
        });

        range.on('end', () => {
            min.dispatchEvent(new Event('change'));
        });
    }

    rangeEvents(priceRange, priceSlider, minPriceInput, maxPriceInput);
    rangeEvents(stockRange, stockSlider, minStockInput, maxStockInput);
}