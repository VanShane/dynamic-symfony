import { debounce } from '../../../common/js/functions';

const search = (searchInput, token, form) => {
    searchInput.addEventListener('keyup', debounce((e) => {
        let data = searchInput.name + '=' + encodeURIComponent(searchInput.value);

        fetch(form.action, {
            method: form.method,
            headers: {
                'X-CSRF-TOKEN': token.name + '=' + token.value,
                'Content-Type': 'application/x-www-form-urlencoded',
                'charset': 'utf-8'
            },
            // mode: 'same-origin',
            body: data
        }).then(response => response.text())
            .then(html => {
                let content = document.createElement('html');
                content.innerHTML = html;
                let newTitle = content.querySelector('header>div.title');
                let oldTitle = document.querySelector('header>div.title');
                let newTable = content.querySelector('main');
                let oldTable = document.querySelector('main');
                oldTitle.innerHTML = newTitle.innerHTML;
                oldTable.innerHTML = newTable.innerHTML;
            });
    }, 350));
}

const adjustPrice = (price) => {
    price.addEventListener('click', (e) => {
        price.dataset.before = `€${price.value}`;
    });
}

const adjustStock = (stock) => {
    stock.addEventListener('click', (e) => {
        stock.dataset.before = stock.value;
    });
}

const removeMessage = () => {
    let success = document.querySelector('.flash-success');
    let warning = document.querySelector('.flash-warning');
    let error = document.querySelector('.flash-error');

    if (success || warning || error) {
        window.addEventListener('click', () => {
            if (success) {
                success.remove();
            }
            if (warning) {
                warning.remove();
            }
            if (error) {
                error.remove();
            }
        })
    }
}

export { search, adjustPrice, adjustStock, removeMessage };