// any CSS you import will output into a single css file (app.css in this case)
import '../styles/app.scss';
// start the Stimulus application
import '../../bootstrap';
// fontawesome
import '@fortawesome/fontawesome-free/js/all';
// noUiSlider
import 'nouislider/dist/nouislider.css';

import Filter from '../../common/js/modules/Filter';
const app = document.getElementById('app');
new Filter(app);
import {removeMessage} from '../js/functions/index';
removeMessage();