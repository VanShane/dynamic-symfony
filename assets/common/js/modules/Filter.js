import { Flipper, spring } from 'flip-toolkit';
import { debounce, throttle } from 'lodash';

/**
 * @property {HTMLElement} pagination
 * @property {HTMLElement} content
 * @property {HTMLElement} sorting
 * @property {HTMLFormElement} form
 */
export default class Filter {

    /**
     * @param {HTMLElement|null} element 
     */
    constructor(element) {
        if (element === null) {
            return;
        }

        this.pagination = element.querySelector('.js-filter-pagination') || null;
        this.content = element.querySelector('.js-filter-content') || null;
        this.sorting = element.querySelector('.js-filter-sorting') || null;
        this.form = element.querySelector('.js-filter-form') || null;
        this.loader = element.querySelector('.js-loader') || null;
        this.totalFound = element.querySelector('.js-filter-totalFound') || null;
        this.qInput = element.querySelector('.js-q-input') || null;

        console.log(this.sorting);
        this.bindEvents();
    }

    /**
     * Ajoute les comportements aux différents éléments
     */
    bindEvents() {
        const linkListener = (e) => {
            e.stopPropagation();
            e.preventDefault();
            if (e.target.tagName === 'A') {
                let url = e.target.getAttribute('href');
                this.loadUrl(url);
            } else if (e.target.tagName === 'svg' || e.target.tagName === 'path') {
                let url = e.target.closest('a').getAttribute('href');
                this.loadUrl(url);
            } else if (e.target.tagName === 'TH') {
                let target = this.sorting.querySelector('#' + e.target.dataset.target);
                let url = target.href;
                this.loadUrl(url);
            }
            return false;
        };

        if (this.sorting !== null) {
            this.sorting.addEventListener('click', linkListener);
        }

        if (this.pagination !== null) {
            this.pagination.addEventListener('click', _.debounce(linkListener, 350));
        }

        if (this.form !== null) {
            this.form.querySelectorAll('input').forEach(input => {
                if (input.type === 'text') {
                    input.addEventListener('keyup', _.debounce(this.loadForm.bind(this), 350));
                } else {
                    input.addEventListener('change', _.debounce(this.loadForm.bind(this), 350));
                }
            });

            if (this.form.querySelector('select') !== null) {
                this.form.querySelector('select').addEventListener('change', _.debounce(this.loadForm.bind(this), 350));
            }
        }

        if (this.qInput) {
            this.qInput.addEventListener('keyup', _.debounce(this.loadForm.bind(this), 350));
        }
    }

    /**
     * load url with form parameters
     * 
     * @param {string} url
     * @return Promise<void>
     */
    async loadUrl(url) {
        this.showLoader();

        const params = new URLSearchParams(url.split('?')[1] || '');

        const response = await fetch(url.split('?')[0] + '?' + params.toString(), {
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            }
        });

        if (response.status >= 200 && response.status < 300) {
            const data = await response.text();

            let html = document.createElement('html');
            html.innerHTML = data;

            this.loadData(html);

            history.replaceState({}, '', url.split('?')[0] + '?' + params.toString());
        } else {
            console.error(response);
        }

        this.hideLoader();
    }

    /**
     * replace old data with new
     * @param {object} data 
     */
    loadData(html) {

        if (this.totalFound) {
            this.totalFound.innerHTML = html.querySelector('.js-filter-totalFound').innerHTML;
        }

        if (this.content) {
            this.flipContent(html.querySelector('.js-filter-content').innerHTML);
        }

        if (this.sorting) {
            this.sorting.innerHTML = html.querySelector('.js-filter-sorting').innerHTML;
        }

        if (this.pagination) {
            this.pagination.innerHTML = html.querySelector('.js-filter-pagination').innerHTML;
        }
    }

    /**
     * construct url parameters from form data & execute loadUrl
     * 
     * @return Promise<void>
     */
    async loadForm() {
        const data = new FormData(this.form);
        const url = new URL(this.form.getAttribute('action') || window.location.href);
        const params = new URLSearchParams();

        data.forEach((value, key) => {
            params.append(key, value);
        });

        if (this.qInput) {
            const qData = new FormData(this.qInput.closest('form'));
            qData.forEach((value, key) => {
                params.append(key, value);
            });
        }

        return this.loadUrl(url.pathname + '?' + params.toString());
    }

    /**
     * remplace les éléments de la grille avec un effet d'animation flip
     * @param {HTMLElement} content : ;
     */
    flipContent(content) {
        const flipper = new Flipper({
            element: this.content
        });

        const springConfig = 'stiff';

        const exitSpring = (el, index, onComplete) => {
            spring({
                config: 'gentle',
                values: {
                    translateY: [0, -20],
                    opacity: [1, 0]
                },
                onUpdate: ({ translateY, opacity }) => {
                    el.style.opacity = opacity;
                    el.style.transform = `translateY(${translateY}px)`;
                },
                onComplete,
            });
        };

        const appearSpring = (el, index) => {
            spring({
                config: 'stiff',
                values: {
                    translateY: [20, 0],
                    opacity: [0, 1]
                },
                onUpdate: ({ translateY, opacity }) => {
                    el.style.opacity = opacity;
                    el.style.transform = `translateY(${translateY}px)`;
                },
                // delay: index * 25
            });
        };

        const check = (content) => {
            content.children.forEach(element => {
                flipper.addFlipped({
                    element: element,
                    flipId: element.id,
                    shouldFlip: false,
                    spring: springConfig,
                    onExit: exitSpring
                });
            })
        };

        const checked = (content) => {
            content.children.forEach(element => {
                flipper.addFlipped({
                    element: element,
                    flipId: element.id,
                    spring: springConfig,
                    onAppear: appearSpring
                });
            })
        };

        if (this.content.classList.contains('content-container')) {
            let content = this.content.children[0].children[1];
            let header = this.content.children[0].children[0];
            if (content !== undefined) {
                check(header);
                check(content);
            } else {
                check(this.content);
            }
        } else {
            check(this.content);
        }

        flipper.recordBeforeUpdate();
        this.content.innerHTML = content;

        if (this.content.classList.contains('content-container')) {
            let content = this.content.children[0].children[1];
            let header = this.content.children[0].children[0];
            if (content !== undefined) {
                checked(header);
                checked(content);
            } else {
                checked(this.content);
            }
        } else {
            checked(this.content);
        }

        flipper.update();
    }

    /** show & hide loader */
    showLoader() {
        this.form.parentElement.classList.add('is-loading');
        this.content.parentElement.classList.add('is-loading');
        this.loader.style.display = 'block';
    }

    hideLoader() {
        this.form.parentElement.classList.remove('is-loading');
        this.content.parentElement.classList.remove('is-loading');
        this.loader.style.display = 'none';
    }
}