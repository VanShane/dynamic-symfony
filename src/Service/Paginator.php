<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator as PaginationPaginator;

class Paginator
{
    private EntityManagerInterface $em;
    private ?int $pageCount = null;

    /**
     * constructor
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * create \Doctrine\ORM\Tools\Pagination\Paginator
     * from querry string
     * 
     * @param integer $page
     * @param integer $limit
     * @param \Doctrine\ORM\Query $query
     * @param bool $fetchJoinCollection
     * 
     * @return @var \Doctrine\ORM\Tools\Pagination\Paginator $paginator
     */
    public function paginate(int $page, int $limit, Query $query, $fetchJoinCollection = false)
    {
        /** @var int $firstResult */
        $firstResult = ($page - 1) *  $limit;

        /** @var \Doctrine\ORM\Query $query */
        $query
            ->setFirstResult($firstResult)
            ->setMaxResults($limit);

        /** @var \Doctrine\ORM\Tools\Pagination\Paginator $paginator */
        $paginator = new PaginationPaginator($query, $fetchJoinCollection);

        $this->pageCount = (int) ceil(count($paginator) / $limit);

        return $paginator;
    }


    /**
     * Get the value of pageCount
     * 
     * @return null|int $pageCount
     */
    public function getPageCount()
    {
        return $this->pageCount;
    }
}
