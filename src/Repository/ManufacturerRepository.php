<?php

namespace App\Repository;

use App\Data\SearchNameData;
use App\Entity\Manufacturer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Manufacturer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Manufacturer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Manufacturer[]    findAll()
 * @method Manufacturer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ManufacturerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Manufacturer::class);
        $this->paginator = $paginator;
    }

    /**
     * find manufacturers in search data
     *
     * @param SearchNameData $search
     * @param integer $limit
     * @return PaginationInterface
     */
    public function findSearch(SearchNameData $search, int $limit): PaginationInterface
    {
        $query = $this->createQueryBuilder('m')
            ->select('m');

        if(!empty($search->q)) {
            $query = $query
                ->andWhere('LOWER(m.name) LIKE LOWER(:q)')
                ->setParameter('q', "%{$search->q}%");
        }

        $query = $query->getQuery();

        return $this->paginator->paginate(
            $query,
            $search->page,
            $limit
        );
    }
}
