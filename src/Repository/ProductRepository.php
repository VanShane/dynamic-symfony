<?php

namespace App\Repository;

use App\Data\SearchData;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Product[]|null  findSearchValue(string $value)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Product::class);
        $this->paginator = $paginator;
    }

    /**
     * find products in SearchData
     *
     * @param SearchData $search
     * @return PaginationInterface
     */
    public function findSearch(SearchData $search, int $limit): PaginationInterface
    {
        $query = $this->getSearchQuery($search)->getQuery();

        return $this->paginator->paginate(
            $query,
            $search->page,
            $limit
        );
    }

    /**
     * find min, max price in SearchData
     *
     * @param SearchData $search
     * @return int[]
     */
    public function findMinMaxPrice(SearchData $search): array
    {
        $results = $this->getSearchQuery($search, true)
            ->select('MIN(p.price) as min', 'MAX(p.price) as max')
            ->getQuery()
            ->getScalarResult();

        if ($results[0]['min'] === null && $results[0]['max'] === null) {
            return [0, 0];
        }

        return [$results[0]['min'], $results[0]['max']];
    }

    /**
     * find min, max stock in SearchData
     *
     * @param SearchData $search
     * @return int[]
     */
    public function findMinMaxStock(SearchData $search): array
    {
        $results = $this->getSearchQuery($search, true)
            ->select('MIN(p.stock) as min', 'MAX(p.stock) as max')
            ->getQuery()
            ->getScalarResult();

        return [$results[0]['min'], $results[0]['max']];
    }


    /**
     * get query from SearchData
     *
     * @param SearchData $search
     * @param boolean $ignore
     * @return QueryBuilder
     */
    private function getSearchQuery(SearchData $search, $ignore = false): QueryBuilder
    {
        // \dd($search);
        $query = $this
            ->createQueryBuilder('p')
            ->select('p, c')
            ->join('p.categories', 'c');

        if (!empty($search->q)) {
            $query = $query
                ->andWhere('LOWER(p.name) LIKE LOWER(:q)')
                ->setParameter('q', "%{$search->q}%");
        }

        if (!empty($search->categories)) {
            $query = $query
                ->andWhere('c.id IN (:categories)')
                ->setParameter('categories', $search->categories);
        }

        if (!empty($search->manufacturer)) {
            $query = $query
                ->andWhere('p.manufacturer = :manufacturer')
                ->setParameter('manufacturer', $search->manufacturer);
        }

        if (!empty($search->minPrice && $ignore === false)) {
            $query = $query
                ->andWhere('p.price >= :minPrice')
                ->setParameter('minPrice', $search->minPrice);
        }

        if (!empty($search->maxPrice && $ignore === false)) {
            $query = $query
                ->andWhere('p.price <= :maxPrice')
                ->setParameter('maxPrice', $search->maxPrice);
        }

        if (!empty($search->minStock && $ignore === false)) {
            $query = $query
                ->andWhere('p.stock >= :minStock')
                ->setParameter('minStock', $search->minStock);
        }

        if (!empty($search->maxStock && $ignore === false)) {
            $query = $query
                ->andWhere('p.stock <= :maxStock')
                ->setParameter('maxStock', $search->maxStock);
        }

        return $query;
    }
}
