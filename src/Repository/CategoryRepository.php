<?php

namespace App\Repository;

use App\Data\SearchNameData;
use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Category::class);
        $this->paginator = $paginator;
    }

    /**
     * find categories in search data
     *
     * @param SearchNameData $search
     * @param integer $limit
     * @return PaginationInterface
     */
    public function findSearch(SearchNameData $search, int $limit): PaginationInterface
    {
        $query = $this->createQueryBuilder('c')
            ->select('c');

        if (!empty($search->q)) {
            $query = $query
                ->andWhere('LOWER(c.name) LIKE LOWER(:q)')
                ->setParameter('q', "%{$search->q}%");
        }

        $query = $query->getQuery();

        return $this->paginator->paginate(
            $query,
            $search->page,
            $limit
        );
    }
}
