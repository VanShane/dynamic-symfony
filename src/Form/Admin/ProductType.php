<?php

namespace App\Form\Admin;

use App\Entity\Category;
use App\Entity\Manufacturer;
use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => false
            ])
            ->add('price', NumberType::class, [
                'label' => false
            ])
            ->add('stock', NumberType::class, [
                'label' => false
            ])
            ->add('manufacturer', EntityType::class, [
                'label' => false,
                'class' => Manufacturer::class,
                'choice_label' => 'name'
            ])
            ->add('categories', EntityType::class, [
                'label' => false,
                'class' => Category::class,
                'choice_label' => 'name',
                'multiple' => true
            ])
            ->add('description', TextareaType::class, [
                'label' => false,
            ])
            ->add('image', FileType::class, [
                'label' => false,
                'multiple' => false,
                // ne pas lier à la bdd
                'mapped' => false,
                'required' => false,

                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png'
                        ],
                        'mimeTypesMessage' => 'this file is not a valid image'
                    ])
                ]
            ])
            ->add('submit', SubmitType::class, []);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
