<?php

namespace App\Form\Admin;

use App\Data\SearchData;
use App\Entity\Category;
use App\Entity\Manufacturer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('q', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Search product'
                ]
            ])
            ->add('categories', EntityType::class, [
                'label' => false,
                'required' => false,
                'class' => Category::class,
                'expanded' => true,
                'choice_label' => 'name',
                'multiple' => true
            ])
            ->add('manufacturer', EntityType::class, [
                'label' => false,
                'required' => false,
                'class' => Manufacturer::class,
                'choice_label' => 'name',
                'multiple' => false,
            ])
            ->add('minPrice', HiddenType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'hidden',
                    'value' => 0
                ]
            ])
            ->add('maxPrice', HiddenType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'hidden',
                    'value' => 0
                ]
            ])
            ->add('minStock', HiddenType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'hidden',
                    'value' => 0
                ]
            ])
            ->add('maxStock', HiddenType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'hidden',
                    'value' => 0
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => SearchData::class,
            'method' => 'GET',
            'allow_extra_fields' => true
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
