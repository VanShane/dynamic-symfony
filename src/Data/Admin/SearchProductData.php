<?php

namespace App\Data\Admin;

use App\Entity\Category;
use App\Entity\Manufacturer;

class SearchProductData
{
    /**
     * @var integer
     */
    public $page = 1;

    /**
     * @var Category[]
     */
    public $categories = [];

    /**
     * @var null|Manufacturer
     */
    public $manufacturer;

    /**
     * @var null|integer
     */
    public $minPrice;

    /**
     * @var null|integer
     */
    public $maxPrice;
    
    /**
     * @var null|integer
     */
    public $minStock;

    /**
     * @var null|integer
     */
    public $maxStock;  
}
