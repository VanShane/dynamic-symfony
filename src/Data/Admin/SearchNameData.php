<?php

namespace App\Data;

class SearchNameData
{

    /** @var integer */
    public $page = 1;

    /** @var string */
    public $q;
}