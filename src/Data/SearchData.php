<?php

namespace App\Data;

use App\Entity\Category;
use App\Entity\Manufacturer;

class SearchData
{
    /**
     * @var integer
     */
    public $page = 1;

    /**
     * @var null|string
     */
    public $q;

    /**
     * @var Category[]
     */
    public $categories = [];

    /**
     * @var null|Manufacturer
     */
    public $manufacturer;

    /**
     * @var null|integer
     */
    public $minPrice;

    /**
     * @var null|integer
     */
    public $maxPrice;
    
    /**
     * @var null|integer
     */
    public $minStock;

    /**
     * @var null|integer
     */
    public $maxStock;
    
}
