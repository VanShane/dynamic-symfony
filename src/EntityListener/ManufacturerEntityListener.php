<?php

namespace App\EntityListener;

use App\Entity\Manufacturer;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\String\Slugger\SluggerInterface;

class ManufacturerEntityListener
{
    private $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public function prePersist(Manufacturer $manufacturer, LifecycleEventArgs $event)
    {
        $manufacturer->computeSlug($this->slugger);
    }

    public function preUpdate(Manufacturer $manufacturer, LifecycleEventArgs $event)
    {
        $manufacturer->computeSlug($this->slugger);
    }
}
