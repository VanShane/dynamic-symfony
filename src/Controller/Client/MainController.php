<?php

namespace App\Controller\Client;

use App\Data\SearchData;
use App\Form\Client\SearchFormType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(name="main_")
 */
class MainController extends AbstractController
{
    /**
     * @Route("/", name="index")
     *
     * @param EntityManagerInterface $em
     * @param integer $page
     * @param Paginator $paginator
     * @return Response
     */
    public function index(ProductRepository $productRepository, Request $request): Response
    {
        $data = new SearchData();
        $data->page = $request->get('page', 1);
        $form = $this->createForm(SearchFormType::class, $data);

        $form->handleRequest($request);

        $pagination = $productRepository->findSearch($data, 6);

        [$min, $max] = $productRepository->findMinMaxPrice($data);

        if ($request->get('ajax')) {
            return new JsonResponse([
                'content' => $this->renderView('client/main/_products.html.twig', ['pagination' => $pagination]),
                'sorting' => $this->renderView('client/main/_sorting.html.twig', ['pagination' => $pagination]),
                'pagination' => $this->renderView('client/main/_pagination.html.twig', ['pagination' => $pagination]),
            ]);
        }

        return $this->render('client/main/index.html.twig', [
            'pagination' => $pagination,
            'min' => $min,
            'max' => $max,
            'form' => $form->createView()
        ]);
    }
}
