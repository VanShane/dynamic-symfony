<?php

namespace App\Controller\Admin;

use App\Data\SearchData;
use App\Entity\Product;
use App\Form\Admin\ProductSearchType;
use App\Form\Admin\ProductType;
use App\Repository\ProductRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 *  @Route("/admin", name="admin_")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/products", name="products", methods={"GET", "POST"})
     *
     * @param ProductRepository $productRepository
     * @param Request $request
     * @param int page
     * @return Response
     */
    public function index(ProductRepository $productRepository, Request $request): Response
    {
        $data = new SearchData();
        $data->page = $request->get('page', 1);

        $form = $this->createForm(ProductSearchType::class, $data);
        $form->handleRequest($request);

        $pagination = $productRepository->findSearch($data, 10);

        $minMaxPrice = $productRepository->findMinMaxPrice($data);
        $minMaxStock = $productRepository->findMinMaxStock($data);

        $nfMessage = 'No corresponding products found, please try again or try adding a new product.';

        if ($request->isXmlHttpRequest()) {
            return $this->render('admin/product/index.html.twig', [
                'pagination' => $pagination,
                'minmaxP' => $minMaxPrice,
                'minmaxS' => $minMaxStock,
                'notFound' => $nfMessage,
                'form' => $form->createView()
            ]);
        }

        return $this->render('admin/product/index.html.twig', [
            'pagination' => $pagination,
            'minmaxP' => $minMaxPrice,
            'minmaxS' => $minMaxStock,
            'notFound' => $nfMessage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/product/new", name="product_new", methods={"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        /** @var \App\Entity\Product $product */
        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $image = $form->get('image')->getData();
            if ($image) {
                $imageName = $fileUploader->upload($image);
                $product->setImage($imageName);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $this->addFlash(
                'success',
                $product->getName() . ' has been added successfully!'
            );
            return $this->redirectToRoute('admin_products');
        }

        return $this->render('admin/product/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/product/{slug}", name="product_show", methods={"GET"})
     *
     * @param Product $product
     * @return Response
     */
    public function show(Product $product): Response
    {
        return $this->render('admin/product/show.html.twig', [
            'product' => $product,
        ]);
    }

    /**
     * @Route("/product/{slug}/edit", name="product_edit", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function edit(Request $request, Product $product, FileUploader $fileUploader): Response
    {

        /** @var \Symfony\Component\Form\FormInterface $form */
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $image = $form->get('image')->getData();
            if ($image) {
                $imageName = $fileUploader->upload($image);
                $product->setImage($imageName);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $this->addFlash(
                'warning',
                $product->getName() . ' has been updated'
            );
            return $this->redirectToRoute('admin_products');
        }

        return $this->render('admin/product/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/product/{slug}", name="product_delete", methods={"POST"})
     * 
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function delete(Request $request, Product $product): Response
    {
        /** @var null|string $name */
        $name = $product->getName();

        if ($this->isCsrfTokenValid('delete' . $product->getSlug(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();

            $this->addFlash(
                'error',
                $name . ' has been removed!'
            );
            return $this->redirectToRoute('admin_products');
        }

        $this->addFlash(
            'warning',
            'Something went wrong, please try again!'
        );

        return $this->redirectToRoute('admin_products');
    }
}
