<?php

namespace App\Controller\Admin;

use App\Data\SearchNameData;
use App\Entity\Category;
use App\Form\Admin\SearchBarType;
use App\Form\Admin\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PhpParser\Node\Stmt\TryCatch;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use function PHPUnit\Framework\throwException;

/**
 * @Route("/admin", name="admin_")
 */
class CategoryController extends AbstractController
{
    /**
     * @Route("/categories", name="categories", methods={"GET", "POST"})
     *
     * @param CategoryRepository $categoryRepository
     * @param Request $request
     * @return Response
     */
    public function index(CategoryRepository $categoryRepository, Request $request): Response
    {
        $data = new SearchNameData();
        $data->page = $request->get('page', 1);

        $form = $this->createForm(SearchBarType::class, $data);
        $form->handleRequest($request);

        $pagination = $categoryRepository->findSearch($data, 5);

        $nfMessage = 'No categories found, please try again or try adding a new category.';

        if ($request->isXmlHttpRequest()) {
            return $this->render('admin/category/index.html.twig', [
                'pagination' => $pagination,
                'notFound' => $nfMessage,
                'form' => $form->createView()
            ]);
        }

        return $this->render('admin/category/index.html.twig', [
            'pagination' => $pagination,
            'notFound' => $nfMessage,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/category/new", name="category_new", methods={"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        /** @var \App\Entity\Category $category */
        $category = new Category();

        /** @var \Symfony\Component\Form\FormInterface $form */
        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Doctrine\Persistence\ObjectManager $em */
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash(
                'success',
                $category->getName() . ' has been added successfully'
            );
            return $this->redirectToRoute('admin_categories');
        }

        return $this->render('admin/category/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/category/{slug}", name="category_show", methods={"GET"})
     *
     * @param Category $category
     * @return Response
     */
    public function show(Category $category, EntityManagerInterface $em): Response
    {
        return $this->render('admin/category/show.html.twig', [
            'category' => $category
        ]);
    }

    /**
     * @Route("/category/{slug}/edit", name="category_edit", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param Category $category
     * @return Response
     */
    public function edit(Request $request, Category $category): Response
    {

        /** @var \Symfony\Component\Form\FormInterface $form */
        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Doctrine\Persistence\ObjectManager $em */
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash(
                'warning',
                $category->getName() . ' has been updated'
            );
            return $this->redirectToRoute('admin_categories');
        }

        return $this->render('admin/category/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/category/{slug}", name="category_delete", methods={"POST"})
     * 
     * @param Request $request
     * @param Category $category
     * @return Response
     */
    public function delete(Request $request, Category $category): Response
    {
        /** @var null|string $name */
        $name = $category->getName();

        if ($this->isCsrfTokenValid('delete' . $category->getSlug(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();

            $this->addFlash(
                'error',
                $name . ' has been deleted'
            );
            return $this->redirectToRoute('admin_categories');
        }

        $this->addFlash(
            'warning',
            'Something went wrong, please try again!'
        );
        
        return $this->redirectToRoute('admin_categories');
    }
}
