<?php

namespace App\Controller\Admin;

use App\Data\SearchNameData;
use App\Entity\Manufacturer;
use App\Form\Admin\ManufacturerType;
use App\Form\Admin\SearchBarType;
use App\Repository\ManufacturerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin_")
 */
class ManufacturerController extends AbstractController
{
    /**
     * @Route("/manufacturers", name="manufacturers", methods={"GET", "POST"})
     *
     * @param ManufacturerRepository $manufacturerRepository
     * @param Request $request
     * @return Response
     */
    public function index(ManufacturerRepository $manufacturerRepository, Request $request): Response
    {
        $data = new SearchNameData();
        $data->page = $request->get('page', 1);

        $form = $this->createForm(SearchBarType::class, $data);
        $form->handleRequest($request);

        $pagination = $manufacturerRepository->findSearch($data, 5);

        $nfMessage = 'No manufacturer found, please try again or try adding a new manufacturer.';

        if ($request->isXmlHttpRequest()) {
            return $this->render('admin/manufacturer/index.html.twig', [
                'pagination' => $pagination,
                'notFound' => $nfMessage,
                'form' => $form->createView()
            ]);
        }

        return $this->render('admin/manufacturer/index.html.twig', [
            'pagination' => $pagination,
            'notFound' => $nfMessage,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/manufacturer/new", name="manufacturer_new", methods={"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        /** @var \App\Entity\Manufacturer $manufacturer */
        $manufacturer = new Manufacturer();

        /** @var \Symfony\Component\Form\FormInterface $form */
        $form = $this->createForm(ManufacturerType::class, $manufacturer);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Doctrine\Persistence\ObjectManager $em */
            $em = $this->getDoctrine()->getManager();
            $em->persist($manufacturer);
            $em->flush();

            $this->addFlash(
                'success',
                $manufacturer->getName() . ' has been added successfully!'
            );
            return $this->redirectToRoute('admin_manufacturers');
        }

        return $this->render('admin/manufacturer/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/manufacturer/{slug}", name="manufacturer_show", methods={"GET"})
     *
     * @param Manufacturer $manufacturer
     * @return Response
     */
    public function show(Manufacturer $manufacturer, EntityManagerInterface $em): Response
    {
        return $this->render('admin/manufacturer/show.html.twig', [
            'manufacturer' => $manufacturer,
        ]);
    }

    /**
     * @Route("/manufacturer/{slug}/edit", name="manufacturer_edit", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param Manufacturer $manufacturer
     * @return Response
     */
    public function edit(Request $request, Manufacturer $manufacturer): Response
    {

        /** @var \Symfony\Component\Form\FormInterface $form */
        $form = $this->createForm(ManufacturerType::class, $manufacturer);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Doctrine\Persistence\ObjectManager $em */
            $em = $this->getDoctrine()->getManager();
            $em->persist($manufacturer);
            $em->flush();

            $this->addFlash(
                'warning',
                $manufacturer->getName() . ' has been updated'
            );
            return $this->redirectToRoute('admin_manufacturers');
        }

        return $this->render('admin/manufacturer/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/manufacturer/{slug}", name="manufacturer_delete", methods={"POST"})
     * 
     * @param Request $request
     * @param Manufacturer $manufacturer
     * @return Response
     */
    public function delete(Request $request, Manufacturer $manufacturer): Response
    {
        /** @var null|string $name */
        $name = $manufacturer->getName();
       
        if ($this->isCsrfTokenValid('delete' . $manufacturer->getSlug(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($manufacturer);
            $em->flush();

            $this->addFlash(
                'error',
                $name . ' has been deleted'
            );
            return $this->redirectToRoute('admin_manufacturers');
        }

        $this->addFlash(
            'warning',
            'Something went wrong, please try again!'
        );

        return $this->redirectToRoute('admin_manufacturers');
    }
}
